import { createReducer } from "reduxsauce";
import { INITIAL_STATE,add,toggle } from './handler'
import { Types } from './action'
/**
 * Reducer
 */
export default createReducer(INITIAL_STATE, {
  [Types.ADD_TODO]: add,
  [Types.TOGGLE_TODO]: toggle
});