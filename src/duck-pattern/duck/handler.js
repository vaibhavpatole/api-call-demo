/**
 * Handlers
 */
export const INITIAL_STATE = [];

export const add = (state = INITIAL_STATE, action) => [...state,
  { id: Math.random(), text: action.text, complete: false }
];

export const toggle = (state = INITIAL_STATE, action) => state.map( todo =>
  todo.id === action.id ? { ...todo, complete: !todo.complete } : todo
);