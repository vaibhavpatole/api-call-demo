import React, { Component } from 'react'
import './fetch.css'

class Fetchdata extends Component {

  constructor(props) {
    super(props)
    this.state = {
       globaldata : {},
       Countries :[]
    }
  }

  chngecountry = (props) => {
    const reqData = this.state.Countries[props]
    alert("country : " + reqData.Country + "\nCountryCode : " + reqData.CountryCode + "\nSlug : " + reqData.Slug + "\nNew Confirmed : " + reqData.NewConfirmed + "\nTotal Confirmed : " + reqData.TotalConfirmed + "\nNew Deths : " + reqData.NewDeaths + "\nTotal Deth : " + reqData.TotalDeaths + "\nNew Recoverd : " + reqData.NewRecovered + "\nTotal Recoverd : " + reqData.TotalRecovered + "\nDate : " + reqData.Date)
  }
  
  
  async componentDidMount() {
    const url = 'https://api.covid19api.com/summary';
    const responce = await fetch(url);
    const data = await responce.json();
    this.setState({globaldata : data.Global,Countries : data.Countries})
  }
  
  render() {
    return (
      <div>
        <div className="book">
          <h5>NewConfirmed : { this.state.globaldata.NewConfirmed }</h5>
          <h5>TotalConfirmed : { this.state.globaldata.TotalConfirmed }</h5>
          <h5>NewDeaths : { this.state.globaldata.NewDeaths }</h5>
          <h5>TotalDeaths : { this.state.globaldata.TotalDeaths }</h5>
          <h5>NewRecovered : { this.state.globaldata.NewRecovered }</h5>
          <h5>TotalRecovered : { this.state.globaldata.TotalRecovered }</h5>
        </div>

        <div className="book1">
          {this.state.Countries.map((country,i) => (
            <button key={i} value={i} onClick={e=>{this.chngecountry(e.target.value)}} >{country.Country}</button>
          ))}
        </div>
      </div>
    )
  }
}

export default Fetchdata
