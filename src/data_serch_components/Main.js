import React, { Component } from 'react'
import 'antd/dist/antd.css';
import { Select } from 'antd';
import { Link } from 'react-router-dom'

const { Option } = Select;

function handleChange(value) {
  alert(`Selected: ${value}`);
}

class Main extends Component {

  constructor(props) {
    super(props)
    this.state = {
       globaldata : {},
       Countries :[]
    }
  }

  async componentDidMount() {
    const url = 'https://api.covid19api.com/summary';
    const responce = await fetch(url);
    const allData = await responce.json();
    this.setState({globaldata : allData.Global,Countries : allData.Countries})
  }

  render() {
    return (
      <div className="container">
        <h3>NewConfirmed : { this.state.globaldata.NewConfirmed }</h3>
          <h3>TotalConfirmed : { this.state.globaldata.TotalConfirmed }</h3>
          <h3>NewDeaths : { this.state.globaldata.NewDeaths }</h3>
          <h3>TotalDeaths : { this.state.globaldata.TotalDeaths }</h3>
          <h3>NewRecovered : { this.state.globaldata.NewRecovered }</h3>
          <h3>TotalRecovered : { this.state.globaldata.TotalRecovered }</h3>
          <Select size="large"  showSearch defaultValue="Select Country" onChange={handleChange} style={{ width: 200 }} filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
            {this.state.Countries.map((country,i) => (
              <Option key={i} value={i}>
                <Link to={`/country/${i}`} >{country.Country}</Link>
              </Option>
            ))}
          </Select>
      </div>
    )
  }
}

export default Main
