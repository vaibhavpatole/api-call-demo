import React,{useState,useEffect} from 'react'
import { Link } from 'react-router-dom';

function DataDisplay({match}) {

  useEffect(() => {
    fetchItem();
    console.log(match.params.id)
  }, []);

  const [ item,setItem ] = useState({});

  const fetchItem = async() =>{
    const url = 'https://api.covid19api.com/summary';
    const responce = await fetch(url);
    const allData = await responce.json();
    const data = allData.Countries[match.params.id];
    setItem(data)
  }

  return (
    <div className="container">
      <h2>Display Data</h2>
      <h3> Country : {item.Country}</h3>
      <h4>CountryCode : {item.CountryCode}</h4>
      <h4>Slug : {item.Slug}</h4>
      <h4>NewConfirmed : {item.NewConfirmed}</h4>
      <h4>TotalConfirmed : {item.TotalConfirmed}</h4>
      <h4>NewDeaths : {item.NewDeaths}</h4>
      <h4>TotalDeaths : {item.TotalDeaths}</h4>
      <h4>NewRecovered : {item.NewRecovered}</h4>
      <h4>TotalRecovered : {item.TotalRecovered}</h4>
      <h4>Date : {item.Date}</h4>
      <Link  to={'/main'}>
        <button>Previous</button>
      </Link>
    </div>
  )
}

export default DataDisplay
