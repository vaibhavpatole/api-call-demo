import React from 'react';
import './App.css';
// import Main from './data_serch_components/Main';
// import DataDisplay from './data_serch_components/DataDisplay';
// import { BrowserRouter as Router,Switch,Route } from 'react-router-dom'

import { Provider } from "react-redux";
import store from "./duck-pattern/store";
import TodoList from './components/TodoList';
// import Button from './components/button/button';

function App() {
  return (
    // <div className="App">
    //   <Button label="click me" />
    // </div>
    <Provider store={store}>
      <TodoList />
    </Provider>
  // <Router>
  //   <div className="App">
	//     <Switch>
  //       <Route path="/" exact component={ Home } />
  //       <Route path="/main" component={ Main } />
  //       <Route path="/country/:id" component={ DataDisplay } />
  //     </Switch>
  //   </div>
	// </Router>
  );
}

// const Home = () =>(
//   <div >
//     <h3>Home Page</h3>
//   </div>
// );

export default App;
